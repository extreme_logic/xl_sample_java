package ph.extremelogic.sample.core.cases.trainstation;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

public class TrainStationBootStrap {
	private HashMap<String, Terminal> terminal;

	private Vector<StationLinks> stationLinks;

	// tracking which terminal are besides each other
	private int terminalIndex = 0;

	private boolean hasLinks = false;

	public TrainStationBootStrap() {
		terminal = new HashMap<String, Terminal>();
		stationLinks = new Vector<StationLinks>();
	}

	public void addTerminal(String terminalName) {
		Terminal terminal = new Terminal();
		terminal.setTerminalIndex(terminalIndex);
		terminal.setTerminalName(terminalName);
		this.terminal.put(terminalName, terminal);
		this.terminalIndex += 1;

	}

	public void addLinks(String terminalName1, String station1, String terminalName2, String station2) {
		StationLinks sl = new StationLinks();
		sl.setTerminalName1(terminalName1);
		sl.setTerminalName2(terminalName2);
		sl.setStation1(station1);
		sl.setStation2(station2);
		stationLinks.add(sl);
	}

	public Terminal getTerminal(String terminalName) {
		return this.terminal.get(terminalName);
	}

	public void display() {
		System.out.println("\n*********************");
		java.util.Iterator<Entry<String, Terminal>> it;

		it = terminal.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Terminal> pair;
			pair = (Map.Entry<String, Terminal>) it.next();
			Terminal t = pair.getValue();
			// it.remove();
			System.out.println(t.getTerminalName());
			// System.out.println(pair.getKey());
			for (Station station : t.getStations()) {
				System.out.print(" -> ");
				System.out.print("[" + station.getPreviousStation() + "]");
				System.out.print(station.getStationName());
				System.out.print("[" + station.getNextStation() + "]");
			}
		}
	}

	public HashMap<String, Vector<String>> getRawStationPath() {
		HashMap<String, Vector<String>> stationPath;
		Vector<String> stations;

		stationPath = new HashMap<String, Vector<String>>();

		java.util.Iterator<Entry<String, Terminal>> it;

		it = terminal.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Terminal> pair;
			pair = (Map.Entry<String, Terminal>) it.next();
			Terminal t = pair.getValue();

			stations = new Vector<String>();
			for (Station station : t.getStations()) {
				stations.add(station.getStationName());
			}
			stationPath.put(t.getTerminalName(), stations);
		}
		return stationPath;
	}

	private HashMap<String, Vector<String>> adjustMap(HashMap<String, Vector<String>> rawStationPath) {
		HashMap<String, Vector<String>> adjustedMap;
		AdjustLinks adjust = new AdjustLinks(stationLinks, rawStationPath);

		adjustedMap = adjust.getAdjustedLinks();

		return adjustedMap;
	}

	public void displayMap(HashMap<String, Vector<String>> rawStationPath) {
		String gap = "---";
		System.out.println();
		java.util.Iterator<Entry<String, Vector<String>>> it;
		HashMap<String, Vector<String>> adjustedPath;
		adjustedPath = adjustMap(rawStationPath);

		it = adjustedPath.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Vector<String>> pair;
			pair = (Map.Entry<String, Vector<String>>) it.next();
			Vector<String> t = pair.getValue();

			for (String station : t) {
				if (null == station) {
					System.out.print(String.format("%6s", gap).replace(' ', '-'));
				} else {
					// System.out.print("[" + station + "]");
					System.out.print(String.format("[%4s]", station));
				}
				System.out.print(gap);
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		TrainStationBootStrap ts = new TrainStationBootStrap();
		ts.addTerminal("T1");
		Terminal t1 = ts.getTerminal("T1");

		Station s = new Station();
		s.setStationName("T1-A");
		t1.addStation(s);

		s = new Station();
		s.setStationName("T1-B");
		t1.addStation(s);

		s = new Station();
		s.setStationName("T1-C");
		t1.addStation(s);

		ts.addTerminal("T2");
		Terminal t2 = ts.getTerminal("T2");

		s = new Station();
		s.setStationName("T2-A");
		t2.addStation(s);

		s = new Station();
		s.setStationName("T2-B");
		t2.addStation(s);

		s = new Station();
		s.setStationName("T2-C");
		t2.addStation(s);

		s = new Station();
		s.setStationName("T2-D");
		t2.addStation(s);

		// ts.display();

		s = new Station();
		s.setStationName("T2-X");
		t2.insertStationAfter("T2-B", s);

		ts.addTerminal("T3");
		Terminal t3 = ts.getTerminal("T3");

		s = new Station();
		s.setStationName("T3-A");
		t3.addStation(s);

		s = new Station();
		s.setStationName("T3-B");
		t3.addStation(s);

		s = new Station();
		s.setStationName("T3-C");
		t3.addStation(s);

		ts.addLinks("T1", "T1-B", "T2", "T2-X");
		ts.displayMap(ts.getRawStationPath());
		
		ts.addLinks("T1", "T1-C", "T2", "T2-D");
		ts.displayMap(ts.getRawStationPath());

		ts.addLinks("T2", "T2-D", "T3", "T3-B");
		ts.displayMap(ts.getRawStationPath());
	}

	/**
	 * @return the hasLinks
	 */
	public boolean isHasLinks() {
		return hasLinks;
	}

	/**
	 * @param hasLinks the hasLinks to set
	 */
	public void setHasLinks(boolean hasLinks) {
		this.hasLinks = hasLinks;
	}

}
