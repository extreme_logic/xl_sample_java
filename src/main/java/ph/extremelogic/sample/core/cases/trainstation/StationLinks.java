package ph.extremelogic.sample.core.cases.trainstation;

public class StationLinks {
	private String terminalName1;
	private String Station1;
	private String terminalName2;
	private String Station2;

	/**
	 * @return the terminalName
	 */
	public String getTerminalName1() {
		return terminalName1;
	}

	/**
	 * @param terminalName the terminalName to set
	 */
	public void setTerminalName1(String terminalName1) {
		this.terminalName1 = terminalName1;
	}

	/**
	 * @return the station1
	 */
	public String getStation1() {
		return Station1;
	}

	/**
	 * @param station1 the station1 to set
	 */
	public void setStation1(String station1) {
		Station1 = station1;
	}

	/**
	 * @return the station2
	 */
	public String getStation2() {
		return Station2;
	}

	/**
	 * @param station2 the station2 to set
	 */
	public void setStation2(String station2) {
		Station2 = station2;
	}

	/**
	 * @return the terminalName2
	 */
	public String getTerminalName2() {
		return terminalName2;
	}

	/**
	 * @param terminalName2 the terminalName2 to set
	 */
	public void setTerminalName2(String terminalName2) {
		this.terminalName2 = terminalName2;
	}
}
