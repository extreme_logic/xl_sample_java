package ph.extremelogic.sample.core.cases.trainstation;

import java.util.Vector;

public class Terminal {
	private String terminalName;
	private int terminalIndex;
	private Vector<Station> station;

	public Terminal() {
		station = new Vector<Station>();
	}

	/**
	 * @return the terminalName
	 */
	public String getTerminalName() {
		return terminalName;
	}

	/**
	 * @param terminalName the terminalName to set
	 */
	public void setTerminalName(String terminalName) {
		this.terminalName = terminalName;
	}

	/**
	 * @return the station
	 */
	public void addStation(Station station) {
		Station prevStation;
		if (0 == this.station.size()) {
			station.setPreviousStation(terminalName);
		} else {
			prevStation = this.station.elementAt(this.station.size() - 1);
			prevStation.setNextStation(station.getStationName());
			this.station.set(this.station.size() - 1, prevStation);
			station.setPreviousStation(prevStation.getStationName());
		}
		this.station.add(station);
	}

	public void insertStationAfter(String name, Station stationToAdd) {
		for (int index = 0; index < this.station.size(); index++) {
			Station station = this.station.get(index);
			String stationName = station.getStationName();
			if (stationName.equalsIgnoreCase(name)) {
				station.setNextStation(stationToAdd.getStationName());
				stationToAdd.setPreviousStation(stationName);

				if (index < this.station.size() - 1) {
					stationToAdd.setNextStation(this.station.get(index + 1).getStationName());
					this.station.get(index + 1).setPreviousStation(stationToAdd.getStationName());
				}
				this.station.insertElementAt(stationToAdd, index + 1);
				break;
			}
		}
	}

	public Vector<Station> getStations() {
		return this.station;
	}

	/**
	 * @return the terminalIndex
	 */
	public int getTerminalIndex() {
		return terminalIndex;
	}

	/**
	 * @param terminalIndex the terminalIndex to set
	 */
	public void setTerminalIndex(int terminalIndex) {
		this.terminalIndex = terminalIndex;
	}
}
