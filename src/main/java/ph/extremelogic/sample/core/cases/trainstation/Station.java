package ph.extremelogic.sample.core.cases.trainstation;

public class Station {
	private String stationName;
	private String linkedToStation;
	private String previousStation;
	private String nextStation;

	/**
	 * @return the stationName
	 */
	public String getStationName() {
		return stationName;
	}

	/**
	 * @param stationName the stationName to set
	 */
	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	/**
	 * @return the linkedToStation
	 */
	public String getLinkedToStation() {
		return linkedToStation;
	}

	/**
	 * @param linkedToStation the linkedToStation to set
	 */
	public void setLinkedToStation(String linkedToStation) {
		this.linkedToStation = linkedToStation;
	}

	/**
	 * @return the previousStation
	 */
	public String getPreviousStation() {
		return previousStation;
	}

	/**
	 * @param previousStation the previousStation to set
	 */
	public void setPreviousStation(String previousStation) {
		this.previousStation = previousStation;
	}

	/**
	 * @return the nextStation
	 */
	public String getNextStation() {
		return nextStation;
	}

	/**
	 * @param nextStation the nextStation to set
	 */
	public void setNextStation(String nextStation) {
		this.nextStation = nextStation;
	}
}
