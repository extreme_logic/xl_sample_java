package ph.extremelogic.sample.core.cases.trainstation;

import java.util.HashMap;
import java.util.Vector;

public class AdjustLinks {
	private Vector<StationLinks> stationLinks;
	private HashMap<String, Vector<String>> rawStationPath;

	public AdjustLinks(Vector<StationLinks> stationLinks, HashMap<String, Vector<String>> rawStationPath) {
		this.stationLinks = stationLinks;
		this.rawStationPath = rawStationPath;
	}

	private Integer getStationIndex(String terminal, String station) {
		Integer index = null;
		Vector<String> terminalStations = rawStationPath.get(terminal);

		for (int rawIndex = 0; rawIndex < terminalStations.size(); rawIndex++) {
			String v = terminalStations.get(rawIndex);
			if (null != v && v.equalsIgnoreCase(station)) {
				index = rawIndex;
				break;
			}
		}

		return index;
	}

	private void moveStationForward(String terminal, String station, int index) {
		Vector<String> stationPath;
		stationPath = rawStationPath.get(terminal);
		int idx = stationPath.indexOf(station);
		for (int y = 0; y < index; y++) {
			stationPath.insertElementAt(null, idx);
		}
		rawStationPath.put(terminal, stationPath);
	}

	public HashMap<String, Vector<String>> getAdjustedLinks() {
		int t1Index;
		int t2Index;

		for (StationLinks it : this.stationLinks) {
			StationLinks sl = it;
			String t1 = sl.getTerminalName1();
			String t2 = sl.getTerminalName2();
			String station1 = sl.getStation1();
			String station2 = sl.getStation2();
			t1Index = getStationIndex(t1, station1);
			t2Index = getStationIndex(t2, station2);
			if (t1Index < t2Index) {
				moveStationForward(t1, station1, t2Index - t1Index);
			} else if (t2Index < t1Index) {
				moveStationForward(t2, station2, t1Index - t2Index);
			}
		}

		return rawStationPath;
	}
}
